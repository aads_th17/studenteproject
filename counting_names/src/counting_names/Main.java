package counting_names;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		List<String> countries = new ArrayList<>();

		int count  = 0;

		Scanner s = new Scanner(System.in);

		System.out.println("enter number of countries: ");
		count = s.nextInt();

		for (int i = 0; i < count; i++) {

			System.out.println("enter country name:  ");
			countries.add(s.next());
		}
		System.out.println(countries);
	}

}
