public class Exercise4 {

	public static void main(String[] args) {

		// Logical Operators
		int agel = 22;
		int money = 800;
		if ((agel > 18) && (money > 500)) {
			System.out.println("Young and Rich!");
		}

		// Logical Operators OR
		int age = 25;
		int moneys = 100;

		if (age > 18 || moneys > 500) {
			System.out.println("Welcome!");
		}

		System.out.println("---------");

		// Logical Operators NOT !
		int ages = 25;
		if (!(ages > 18)) {
			System.out.println("Too Young");
		} else {
			System.out.println("Welcome");
		}

		System.out.println("---------");

		int x = 1;
		int y = 2;
		if (x != y) {
			System.out.println("Print something");
		}

		System.out.println("---------");

		// There is another operator called XOR (also called Exclusive OR).
		// It functions like a normal OR except when both conditions are true.
		boolean a = true;
		boolean b = true;

		if (a == true ^ b == true) {
			System.out.println("A single line");
		} else {
			System.out.println("What?");
		} // it won't print a line since both conditions equals false

		/*
		 * explanation for XOR , suppose there are 2 boolean values a,b then XOR will
		 * return {(!a && b)or(a &&!b)}
		 */

		System.out.println("---------");

	}

}
