import java.util.Scanner;

public class Exercise6 {

	public static void main(String[] args) {

		// While loops
		int x = 3;

		while (x > 0) {
			System.out.println(x);
			x--;
		}

		System.out.println("---------");

		int y;
		Scanner scn = new Scanner(System.in);
		System.out.println("Enter a number:");
		y = scn.nextInt();

		System.out.println("your entered number: " + y);

		while (y > 0)
			System.out.println("decrement sequence " + y + " is " + --y);

		// Can we write while loops without curly brackets?

		System.out.println("---------");

		int o = 8;
		while (o <= 16) {
			System.out.println("Java rocks!");
			o++;
		}

		System.out.println("---------");

		int q = 10;
		int counter = 3;
		while (q > 4) {
			System.out.println(q);
			q--; // decrement by 1 here
			if (q == 5) {
				q += 5; // when x reaches 5 increase value by 5, so the whole
				counter--;
				// How to make a loop run only for 3 times?
			}
			if (counter == 0) {
				break;
			}
		}

		System.out.println("---------");

		int time;
		Scanner timeScan = new Scanner(System.in);
		System.out.println("Set timer:");
		time = timeScan.nextInt();
		while (time >= 0) {
			time--;
			if (time >= 0) {
				System.out.println(time);

			}
			// How to start the count down after the input?
		}
		System.out.println("BOOM!");

		System.out.println("---------");

		// for loops
		for (int c = 2; c <= 10; c = c * c) {
			System.out.println(c);
		}

		System.out.println("---------");

		for (int t = 0; t <= 10; t++)
			System.out.println(t++);
		// Can we write for loops without curly brackets?

		System.out.println("---------");

		// do...while loops
		int u = 1;
		do {
			System.out.println(u);
			u++;
		} while (u < 5);

		System.out.println("---------");

		String name;
		int answer;
		Scanner scan = new Scanner(System.in);
		do {
			System.out.println("Please insert your name:");
			name = scan.next();
			System.out.println("Hello " + name);
			System.out.println();
			System.out.println("Would you like to change your name? \nEnter 1 for yes or 2 for no");
			answer = scan.nextInt();
		} while (answer == 1);
		// How to stop the last three prints execution?

	}

}
