import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {

		// Assignment Operators
		int num1 = 4;
		int num2 = 8;
		num2 -= num1--;
		num2 += num1;
		System.out.println(num2);
		num2 *= num1 + num2;
		// ?
		System.out.println(num2);

		System.out.println("---------");

		int num3 = 5;
		int num4 = 2;
		num4 *= num3 + num4;
		System.out.println(num4);

		System.out.println("---------");

		// Strings
		String var;
		var = "Hello";
		System.out.println(var);

		System.out.println("---------");

		String firstName, lastName;
		firstName = "David";
		lastName = "Williams";

		System.out.println("My name is " + firstName + " " + lastName);

		System.out.println("---------");

		// Getting User Input (Scanner)
		Scanner sc = new Scanner(System.in);
		/*
		 * Declaring new "Scanner" object with name "sc" that uses System.in object that
		 * can read user input - commonly from keyboard.
		 */

		System.out.println("Enter anything to continue:");
		String st = sc.nextLine();
		/*
		 * Declaring "String" with name "st" that is equal to user input that "sc" named
		 * Scanner will get.
		 */

		System.out.println(st);
		// Printing the "st" named String what we declared above.

		System.out.println("---------");

		Scanner variable = new Scanner(System.in);
		String food;
		System.out.println("What is your favourite food?");
		food = variable.nextLine();
		System.out.println("Never heard that before!");

		System.out.println("---------");

		String A = " the Great";
		System.out.println("Type your name to get a title:");
		Scanner B = new Scanner(System.in);
		System.out.println(B.next() + A);

		System.out.println("---------");

		String yourName = "Empty";
		int yourAge = 999;
		double yourGPA = 99.99;

		Scanner getInfo = new Scanner(System.in);

		System.out.println("Please fill the sections in order:");
		System.out.println("Your Name: " + yourName + "| Your Age: " + yourAge + "| Your GPA: " + yourGPA);

		yourName = getInfo.nextLine();

		yourAge = getInfo.nextInt();

		yourGPA = getInfo.nextDouble();

		System.out.println("Your Name: " + yourName + "| Your Age: " + yourAge + "| Your GPA: " + yourGPA);

		System.out.println("---------");

		System.out.println("Enter the price of the item?");
		Scanner keyboard = new Scanner(System.in);

		double price = keyboard.nextDouble();

		System.out.println("The price you've entered is " + price);

		double newPrice = price * 0.8;
		System.out.println("Once the 20% discount is applied, the item will cost " + newPrice);

	}

}
