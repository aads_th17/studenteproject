class Exercise1 {
	public static void main(String[] args) {

		String name = "David ";
		int age = 42, birthdate = 1998;
		double score = 15.9;
		char group = 'Z';

		String concatenatedString=age+" ";
		System.out.println(name + age + " " + birthdate + score); 
		System.out.println(group);
		System.out.print(group);

		float x = (float) Math.pow(3, 2);

		int y = 2 * 2;

		int sum1 = 50 + 10;
		int sum2 = sum1 + 66;
		int sum3 = sum2 + sum2;

		float sum4 = 5 / 1;

		String z = "x" + "y";

		int test = 5;
		test = test + 1;

		System.out.println(x);
		System.out.println("---------");
		System.out.println(y);
		System.out.println("---------");
		System.out.println(sum3);
		System.out.println("---------");
		System.out.println(sum4);
		System.out.println("---------");
		System.out.println(z);
		System.out.println("---------");
		System.out.println(test++);
		System.out.println(test);
		System.out.println(++test);
		System.out.println(test);
	}
}