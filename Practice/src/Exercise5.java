import java.util.Scanner;

public class Exercise5 {

	public static void main(String[] args) {

		// The Switch Statement
		int day = 3;
		switch (day) {
		case 1:
			System.out.println("Monday");
			break;
		case 2:
			System.out.println("Tuesday");
			break;
		case 3:
			System.out.println("Wednesday");
			break;
		}

		System.out.println("---------");

		int days = 6;
		switch (days) {
		case 1:
			System.out.println("Monday");
			break;
		case 2:
			System.out.println("Tuesday");
			break;
		case 3:
			System.out.println("Wednesday");
			break;
		case 6:
		case 7:
			System.out.println("Holidays");
			break;
		}

		System.out.println("---------");

		int dayx = 100;
		switch (dayx) {
		case 1:
			System.out.println("Monday");
			break;
		case 2:
			System.out.println("Tuesday");
			break;
		case 3:
			System.out.println("Wednesday");
			break;
		default:
			System.out.println("Invalid day");
			break;
		}

		System.out.println("---------");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a vowel:");

		String name = scanner.next();
		for (char character : name.toCharArray()) {

			switch (character) {
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
				System.out.println("Vowel");
				break;
			default:
				System.out.println("Not a vowel!");
			}
		}

		System.out.println("---------");

		int dayq;
		Scanner dayqnumber = new Scanner(System.in);
		System.out.println("Enter a day:");
		dayq = dayqnumber.nextInt();
		if (dayq > 0 && dayq < 8) {
			switch (dayq) {
			case 6:
				System.out.println("Saturday");
				break;
			case 7:
				System.out.println("Sunday");
				break;
			default:
				System.out.println("Weekday");
			}
		} else {
			System.out.println("Please enter a valid day!");
		}

	}

}
