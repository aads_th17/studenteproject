import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {

		// Conditional statements
		int x = 7;
		if (x < 42) {
			System.out.println("Hi");
		}

		System.out.println("---------");

		Scanner nice = new Scanner(System.in);
		System.out.println("Welcome to the best cafe in town!");
		System.out.println();
		System.out.println("Please enter your age for a 20% discount:");

		int age = nice.nextInt();

		if (age < 18) {
			System.out.println("We're sorry. You're qualified for the discount :(");
		} else {
			System.out.println("Congratulations! You're too old for the discount. Pay the full price please :)");
		}

		System.out.println("---------");

		Scanner input = new Scanner(System.in);
		System.out.print("Please enter to login! \nName = ");
		String login = input.next();
		System.out.print("Password = ");
		String password = input.next();

		if (login.equals("abc") && password.equals("1234")) {
			System.out.println("Login Success!");
		} else {
			System.out.println("Login Fail!");
		}

		System.out.println("---------");

		// Nested if Statements
		int agex;

		Scanner agescan = new Scanner(System.in);
		System.out.println("Please enter your age:");

		agex = agescan.nextInt();
		System.out.println("Your Age:" + " " + agex);

		if (agex > 0) {
			if (agex < 16) {
				System.out.println("Too Young!");
			} else {
				System.out.println("Not young, but not old too!");
			}
		} else {
			System.out.println("Error!");
		}

		System.out.println("---------");

		int aged;
		Scanner ageScan = new Scanner(System.in);
		System.out.println("Enter your age to register:");
		aged = ageScan.nextInt();

		if (aged < 0) {
			System.out.println("No Minus Age :D");
		}
		if (0 < aged && aged < 70) {
			if (aged > 18) {
				System.out.println("Not young, but not old too!!");
			} else {
				System.out.println("Too Young!");
			}
		} else {
			System.out.println("Wow! Too Old");
		}

		System.out.println("---------");

		// else if Statements
		System.out.println("Enter your level:");
		Scanner lvlscan = new Scanner(System.in);

		int lvl = lvlscan.nextInt();

		System.out.println("Your lvl: " + lvl);

		if (lvl <= 0) {
			System.out.println("lvl Not Found!");
		} else if (lvl <= 50) {
			System.out.println(lvl + " is Beginner lvl: Unlocked The Achievement! -Newbie-");
		} else if (lvl < 100) {
			System.out.println(lvl + " is Pro lvl: Unlocked The Achievement -Veteran-");
		} else {
			System.out.println(lvl + " is God lvl: All Achievements Unlocked!");
		}

		System.out.println("---------");

	}

}
