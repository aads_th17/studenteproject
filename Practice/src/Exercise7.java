import java.util.Arrays;
import java.util.Scanner;

public class Exercise7 {

	public static void main(String[] args) {

		// Loop control statements Break & Continue
		int x = 3;

		while (x > 0) {
			System.out.println(x);
			if (x == 4) {
				break;
			}
			x++;
		}

		System.out.println("---------");

		for (int y = 10; y <= 40; y = y + 10) {
			if (y == 30) {
				continue;
			}
			System.out.println(y);
		}

		System.out.println("---------");

		int fugitive = 0;

		for (int prisoner = 0; prisoner < 6; prisoner++) // Prison transfer of 5 prisoners.
		{
			if (prisoner == 3) {
				fugitive = prisoner; // Prison cameras.
				continue; // Prisoner accomplice.
			}

			System.out.println(prisoner);
		}

		System.out.println("Transfer error! Checked the prison cameras. The fugitive is " + fugitive + ".");

		System.out.println("---------");

		// Arrays
		int[] arr = new int[5];
		System.out.println(Arrays.toString(arr));

		System.out.println("---------");

		String[] myNames = { "A", "B", "C", "D" };
		System.out.println(myNames[2]);

		System.out.println("---------");

		String[] myName = { "A", "B", "C", "D" };

		for (int i = 0; i <= 3; i++) {
			System.out.println(myName[i]);
		}

		System.out.println("---------");

		int[] intArr = new int[5];
		System.out.println(intArr.length);

		System.out.println("---------");

		System.out.println("Enter a day:");
		String[] weekDays = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
		Scanner keyboard = new Scanner(System.in);
		int day = keyboard.nextInt();
		System.out.println(weekDays[day - 1]);

		System.out.println("---------");

		int[] myArr = { 6, 42, 3, 7 };
		int sum = 0;
		for (int q = 0; q < myArr.length; q++) {
			sum += myArr[q];
		}
		System.out.println(sum);
		// The condition of the for loop is x<myArr.length, as the last element's index
		// is myArr.length-1.

		System.out.println("---------");

		int[] myArra = { 6, 42, 3, 7 };
		int summ = 0;
		for (int z : myArra) {
			summ += z;
		}
		System.out.println(summ);

		System.out.println("---------");

		int[] primes = { 2, 3, 5, 7 };

		for (int t : primes) {
			System.out.println(t);
		}

		System.out.println("---------");

		int[][] sample = { { 1, 2, 3 }, { 4, 5, 6 } };
		int b = sample[1][0];
		System.out.println(b);

		System.out.println("---------");

		int[][] same = { { 1, 2, 3 }, { 4, 5, 6 } };
		for (int[] i : same) {
			for (int j : i) {
				System.out.println(j);
			}
		}

		System.out.println("---------");

		int[][] sale = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		for (int f[] : sale) {
			for (int r : f) {
				System.out.print(r);

			}
			System.out.println();
			System.out.println();

		}

		// How the output can be like this?:
		// 123
		// 456
		// 789

	}

}
