class Exercise8 {

	// Classes and Objects
	// Methods
	static void sayHell(String name, String add) {
		System.out.println(name + add + "Heaven");
	}

	static void doSomething(int x) {
		System.out.println(x * x);
	}

	public static void main(String[] args) {
		sayHell("World ", "is ");
		sayHell("World ", "is not ");
		sayHell("Why ", "World is not ");
		System.out.println("-------------");
		doSomething(4);
	}

}